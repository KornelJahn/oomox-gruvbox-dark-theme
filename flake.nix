{
  description = "Dark GTK and icon themes with gruvbox colors, created using Oomox.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system: 
      let 
        pkgs = import nixpkgs { inherit system; };
        myPackage = pkgs.callPackage ./. {};
        pname = myPackage.pname;
      in rec {
        # For `nix build`
        packages.${pname} = myPackage;
        defaultPackage = packages.${pname};
        # For nixpkgs packages
        legacyPackages.${pname} = packages.${pname};
      }
    );
}
