{ stdenv
, gtk3
, gtk-engine-murrine
, breeze-icons
, gnome-icon-theme
, hicolor-icon-theme
}:

stdenv.mkDerivation rec {
  pname = "oomox-gruvbox-dark-theme";
  version = "20210209";

  src = ./.;

  nativeBuildInputs = [ gtk3 ];

  propagatedUserEnvPkgs = [
    gtk-engine-murrine
    breeze-icons
    gnome-icon-theme
    hicolor-icon-theme
  ];

  installPhase = ''
    mkdir -p $out/share
    mv {themes,icons} $out/share

    for theme in $out/share/icons/*; do
      gtk-update-icon-cache $theme
    done
  '';

  dontFixup = true;
  dontStrip = true;

  meta = with stdenv.lib; {
    description = "Dark GTK2/3 theme and icon theme with gruvbox colors (generated using Oomox).";
    platforms = platforms.linux;
  };
}
